# Milestone 3: Feature implementation and support

## Overview

This document outlines the work completed around the implementation and integration of connectionless credential issuance and connectionless verification of verifiable credentials (VCs) within the Ahau wallet application repositories. The repositories include the ssb-atala-prism integration module to commuincate with the wallet P2P backend and API and the Ahau front application implementing the issuance and presentation process for tribal registrations. This milestone is the third and final milestone for the [Grow AtalaPrism: Holder connection requests](https://milestones.projectcatalyst.io/projects/1100149/milestones/3)

# Milestone Evidence Links

1. New application release available for download
    - https://github.com/Ahau-NZ/Ahau/releases

2. Repositories demonstrating any issue responses 
    - https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/issues
    - https://gitlab.com/matou-collective/ahau/-/issues

3. Documentation available 
    - https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/merge_requests/14

4. Commits available inside the project repositories 
    
    #### SSB-Atala-Prism Code

    1. https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/merge_requests/11
    2. https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/merge_requests/12
    3. https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/merge_requests/13
    4. https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/merge_requests/15

    #### Ahau Code

    1. https://gitlab.com/matou-collective/ahau/-/merge_requests/6


5. Video link
    - https://youtu.be/f1HvrPQmkaw

6. Presentation link
    - https://docs.google.com/presentation/d/1kmCo9pTRmNcPX5g0pQgvLvECnRy61sUltjX0jbsyBfw/edit?usp=sharing
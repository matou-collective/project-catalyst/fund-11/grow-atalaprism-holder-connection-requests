# Āhau Tribal Registration Process

**Tribal Member registering to join a tribe in Āhau:**

  ```mermaid
  stateDiagram-v2

  q1: Does the tribe verify credentials?
  q2: Do you have a credential that is relevant?
  q3: Does the tribe issue credentials?

  a1: Send connection request and registration
  a2: Done

  state d1 <<choice>>
  state d2 <<choice>>
  state d3 <<choice>>

  [*] --> q1

  %% does the tribe verify credentials
  %% decision
  q1 --> d1
  d1 --> q2: yes
  d1 --> q3: no


  %% do you have a credential that is relevant?
  q2 --> d2
  d2 --> a1: yes
  d2 --> a2: no

  %% does the tribe issue credentials
  q3 --> d3
  d3 --> a1: yes
  d3 --> a2: no
  ```

  **Tribal Kaitiaki reviewing a registration to join a tribe in Āhau:**

  ```mermaid
  stateDiagram-v2

  q1: Does the tribe verify credentials?
  q2: Does the tribe issue credentials?

  q4: Is the registration accepted?

  UM1: Do we have a connection?

  a1: Accept connection request
  a2: Request presentation and await response
  a3: Issue credential
  a4: Add tribal member
  a5: Accept connection request

  state d1 <<choice>>
  state d2 <<choice>>
  state d3 <<choice>>
  state d4 <<choice>>

  [*] --> q1

  q1 --> d1
  d1 --> a1: yes
  d1 --> q4: no


  a1 --> a2
  a2 --> q4


  %% does the tribe issue credentials?
  q2 --> d4
  d4 --> UM1: yes
  d4 --> a4: no

  %% do we have a connection?
  %% a3 --> UM1
  UM1 --> d3
  d3 --> a4: yes
  d3 --> a5: no


  a4 --> DONE
  a5 --> a3
  a3 --> a4

  q4 --> d2

  d2 --> DONE: no
  d2 --> q2: yes
  ```

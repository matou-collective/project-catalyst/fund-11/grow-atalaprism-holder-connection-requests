# Product Development Plan

## Stages

- Plan, Analyze, Design
  - Product Requirements Document
  - Product Architecture Design
  - Product Development Plan ←THIS DOC
- Build, Test
- Deploy

## :calendar: (20th May - 10 July 2024)

### Update Cloud Agents

1. [`identus-cloud-agent`] issuer update
    - Description: Allow issuers to auto-accept connections
    - Tasks:
        - [ ] Implementation
        - [ ] Update docs
        - [ ] Add tests
        - [ ] Open a PR
        - [ ] Merge and publish once approved
2. [`identus-cloud-agent`] verifier update
    - Description: Allow verifiers to auto-accept connections
    - Tasks:
        - [ ] Implementation
        - [ ] Update docs
        - [ ] Add tests
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved

### Deploy Cloud Agents

1. [`identus-cloud-agent`] issuer deployment
    - Description: Deploy changes to the issuer on a cloud server
    - Tasks:
        - [ ] Start with a local deployment
        - [ ] Start up a cloud server or update an existing one with the new changes to the issuer
        - [ ] Configure
        - [ ] Deploy
        - [ ] Document steps for both local and cloud deployment
        - [ ] Document any issues in the deployment process
2. [`identus-cloud-agent`] verifier deployment
    - Description: Deploy changes to the verifier on a cloud server
    - Tasks:
        - [ ] Start with a local deployment
        - [ ] Start up a cloud server or update an existing one with the new changes to the verifier
        - [ ] Configure
        - [ ] Deploy
        - [ ] Document steps for both local and cloud deployment
        - [ ] Document any issues in the deployment process

### Update Atala Prism Wallet SDK

1. [`@atala/prism-wallet-sdk`] initiate connection update
    - Description: Allow prism wallet sdk holders to initiate connections with cloud agents
    - Tasks:
        - [ ] Configure tests to work with new cloud agents
        - [ ] Implementation
        - [ ] Fix and update any failing tests
        - [ ] Add tests
        - [ ] Update docs
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered

### Integrate changes into Āhau (backend)

1. [`ssb-atala-prism`] update to latest SDK version
    - Description: Update [`@atala/prism-wallet-sdk`] to version (v5.1.0). 
    - Tasks:
        - [ ] Update the [`@atala/prism-wallet-sdk`] package
        - [ ] Integrate any new changes
        - [ ] Fix and update any failing tests
        - [ ] Add tests
        - [ ] Update docs, including breaking changes (CHANGELOG)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered
2. [`ssb-atala-prism`] integration update #1
    - Description: Integrate [`@atala/prism-wallet-sdk`] initiate connection update
    - Tasks:
        - [ ] Configure tests to work with new cloud agents
        - [ ] Integrate any new changes
        - [ ] Fix and update any failing tests
        - [ ] Add tests
        - [ ] Update docs, including breaking changes (CHANGELOG)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered
3. [`ssb-atala-prism`] integration update #2
    - Description: Integrate [`identus-cloud-agent`] issuer update. Create a new API endpoint for initiating connection with an issuer agent.
    - Tasks:
        - [ ] Configure tests to work with new cloud issuer
        - [ ] Integrate any new changes
        - [ ] Implement new API endpoint
        - [ ] Fix and update any failing tests
        - [ ] Add tests
        - [ ] Update docs, including breaking changes (CHANGELOG)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered
4. [`ssb-atala-prism`] integration update #3
    - Description: [`identus-cloud-agent`] verifier update. Create a new API endpoint for initiating connection with a verifier agent.
    - Tasks:
        - [ ] Configure tests to work with new cloud verifier
        - [ ] Integrate any new changes
        - [ ] Implement new API endpoint
        - [ ] Fix and update any failing tests
        - [ ] Add tests
        - [ ] Update docs, including breaking changes (CHANGELOG)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered
5. [`ssb-ahau`] integration update
    - Description: Integrate [`ssb-atala-prism`] updates
    - Tasks:
        - [ ] Update relevant packages
        - [ ] Configure tests to work with new cloud agents (if needed)
        - [ ] Implementation (if needed)
        - [ ] Fix and update any failing tests
        - [ ] Add tests
        - [ ] Update docs, including breaking changes (CHANGELOG)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered

---

## :calendar: (11th July - 20th July 2024)

### Milestone 2 Proof of Achievement (PoA) Reporting & Community Review
- Submit POA
    - [ ] Create Slide Deck with links to Milestone 2 evidence
    - [ ] Create a video showing highlights and evidence
    - [ ] Submit all PoA requirements in Project Catalyst
- Community Review
    - [ ] Respond to PoA feedback and request for information
    - [ ] Receive confirmation of Approval

---

## :calendar: (20th July - 10th August 2024)

### Integrate changes into Āhau (frontend):

1. `ahau` integration update
    - Description: Integrate [`ssb-ahau`] and [`ssb-atala-prism`] updates into the [`ahau`] frontend
    - Tasks:
        - [ ] Update relevant packages
        - [ ] Add new methods to call the new API endpoints from [`ssb-atala-prism`] for initiating connections to issuer and verifier cloud agents
        - [ ] Configure tests to work with new cloud agents (if needed)
        - [ ] Configure environment to work with new cloud agents (if needed)
        - [ ] Update docs (if needed)
        - [ ] Open a PR for review
        - [ ] Merge once approved
        - [ ] Document any issues encountered

### Āhau use case demonstration - Part 1

Update the tribal registration process to handle applicants generating a connection request to be sent to the relevant cloud agent when their tribal registration is sent.

1. [`ssb-tribes-registration`] update
    - Description: Update tribal registration to handle the applicants connection state.
    - Tasks:
        - [ ] Update relevant packages
        - [ ] Implementation
        - [ ] Update docs (if needed)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered
2. [`@ssb-graphql/tribes`] update
    - Description: Integrate the [`ssb-tribes-registration`] update. Update tribal registrations to handle the applicants connection state.
    - Tasks:
        - [ ] Update relevant packages
        - [ ] Implementation
        - [ ] Update docs (if needed)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered
3. [`ssb-ahau`] update 
    - Description: Integrate the [`ssb-tribes-registration`] and [`@ssb-graphql/tribes`] updates.
    - Tasks:
        - [ ] Update relevant packages
        - [ ] Implementation
        - [ ] Update docs (if needed)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered
4. [`ahau`] integration update
    - Description: Integrate the [`ssb-ahau`] and [`ssb-tribes-registration`] updates.
    - Tasks:
        - [ ] Update relevant packages
        - [ ] Implementation
        - [ ] Update docs (if needed)
        - [ ] Open a PR for review
        - [ ] Merge and publish once approved
        - [ ] Document any issues encountered
5. [`ahau`] UI #1 - verifier
    - Description: Update the tribal registration process and UI to handle tribal applicants who need to initiate connection with a **verifier agent**.
    - Tasks:
        - [ ] Implementation
        - [ ] Update docs (if needed)
        - [ ] Write guidelines for smoke testing
        - [ ] Open a PR for review
        - [ ] Merge once approved
        - [ ] Document any issues encountered
6. [`ahau`] UI #2 - issuer
    - Description: Update the tribal registration process and UI to handle tribal applicants who need to initiate connection with an **issuer agent**.
    - Tasks:
        - [ ] Implementation
        - [ ] Update docs (if needed)
        - [ ] Write guidelines for smoke testing
        - [ ] Open a PR for review
        - [ ] Merge once approved
        - [ ] Document any issues encountered

### Āhau use case demonstration - Part 2

Update the tribal registration process to handle the tribal kaitiaki approving the registration after the tribal applicant has established connection with the relevant cloud agent.

1. [`ahau`] UI #3 - verification and approval
    - Description: Update the tribal registration process and UI to handle the tribal kaitiaki verifying credentials, after a tribal applicant has established connection and approving the registration.
    - Tasks:
        - [ ] Implementation
        - [ ] Update docs (if needed)
        - [ ] Write guidelines for smoke testing
        - [ ] Open a PR for review
        - [ ] Merge once approved
        - [ ] Document any issues encountered
2. [`ahau`] UI #4 - issuance and approval
    - Description: Update the tribal registration process and UI to handle the tribal kaitiaki approving the tribal registration and issuing credentials, after a tribal applicant has established connection.
    - Tasks:
        - [ ] Implementation
        - [ ] Update docs (if needed)
        - [ ] Write guidelines for smoke testing
        - [ ] Open a PR for review
        - [ ] Merge once approved
        - [ ] Document any issues encountered

### Āhau use case demonstration - Deployment

1. [`ahau`] deployment
    - Description: Release a new version of Āhau online
    - Tasks:
        - [ ] Update the CHANGELOG
        - [ ] Create a new version (tag)
        - [ ] Create a new release
        - [ ] Publish the release
        - [ ] Publish the link to the release

---

## :calendar: (11th August - 20th August 2024)

### Final Milestone Proof of Achievement (PoA) Reporting & Community Review
- Submit POA
    - [ ] Create Slide Deck with links to Final Milestone evidence
    - [ ] Create a video showing highlights and evidence
    - [ ] Create the written report
    - [ ] Submit all PoA requirements in Project Catalyst
- Community Review
    - [ ] Respond to PoA feedback and request for information
    - [ ] Receive confirmation of Approval


<!-- Markdown References-->

[`identus-cloud-agent`]: https://github.com/hyperledger/identus-cloud-agent
[`atala-prism-mediator`]: https://github.com/input-output-hk/atala-prism-mediator
[`@atala/prism-wallet-sdk`]: https://github.com/input-output-hk/atala-prism-wallet-sdk-ts

[`ssb-atala-prism`]: https://gitlab.com/ahau/lib/ssb-plugins/ssb-atala-prism
[`ssb-tribes-registration`]: https://gitlab.com/ahau/lib/ssb-plugins/ssb-tribes-registration
[`@ssb-graphql/tribes`]: https://gitlab.com/ahau/lib/graphql/ssb-graphql-tribes
[`ssb-ahau`]: https://gitlab.com/ahau/lib/ssb-plugins/ssb-ahau
[`ahau`]: https://gitlab.com/ahau/ahau

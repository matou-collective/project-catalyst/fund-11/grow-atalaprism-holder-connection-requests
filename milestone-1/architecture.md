# Architecture

The architecture consists of several key components:

1. **Āhau**: A decentralised data management app, which also integrates the wallet into its Tribal registration process, allowing the creation, management and verification of digital identities by Tribal Members and Tribal Kaitiaki.
2. **Pātaka**: An Āhau tool which allows Āhau users to sync databases with each other when online.
3. **Atala Prism Wallet SDK Agents (Holders):** A tool which enables developers to integrate decentralised identity management capabilities into their applications and services. This module is also responsible for initating connection requests with cloud agents on behalf of holders.
4. **Issuer Agents (Cloud Agent)**: A cloud agent that is responsible for handling connection invitations and digital identity management for wallet SDK agents, particularly with the issuance of credentials and handling issuance requests.
5. **Verifier Agents (Cloud Agent):** A cloud agent that is responsible for handling connection invitations and digital identity verification for wallet SDK agents, particularly with the verification of credentials and handling presentation requests.
6. **Mediator**: Responsible for the secure communication and interaction between various entities such as holders and cloud agents.

## **Key Features**

1. **Connection Initiation by Holders:** Holders can now generate connection invitations themselves and send them to cloud agents.
2. **Auto-Acceptance by Cloud Agents:** Cloud agents automatically accept incoming connection requests from Wallet SDK Agents, streamlining the connection process.
3. **Use Case Demonstration:**  Āhau integration demonstrates the practical applications of the enhanced features, such as the issuance and verification of credentials online, within the tribal registration processes.
4. **Testing and Quality Assurance:** Rigorous testing ensures that the system meets performance, security, and reliability requirements.

## **High-Level Architecture Diagrams**

![System Architecture A](../assets/system-architecture-2.png)

![System Architecture B](../assets/system-architecture-1.png)

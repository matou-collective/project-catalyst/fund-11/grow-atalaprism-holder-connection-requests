# Milestone 2: Develop the new connection request feature

## Overview

This document outlines the work completed around the implementation and integration of connectionless credential issuance and connectionless verification of verifiable credentials (VCs) within the SDK and Agent repositories. It provides the background on why we shifted our approach from holder-initiated connections to leveraging the connectionless model, and highlights the contributions, testing efforts, and documentation updates made throughout the project.

---

## 1. Context: The Shift from Holder-Initiated Connections to Connectionless Issuance and Verification

For an in-depth look at the shift from holder-initiated connections to connectionless issuance and verification, please refer to [the Transition documentation](./development-documentation/transition-connectionless-issuing.md)

### 1.1 Initial Plan: Holder-Initiated Connections

Our original goal was to develop functionality to support holder-initiated connections, where the holder of credentials would establish a connection with an issuer or verifier agent before any credential issuance or verification could take place. This involved creating new connection flows within the SDK and agent, focusing on seamless auto-acceptance of connection requests.

For more information on this design please see the [holder connections documentation folder](./holder-connections-docs/)

### 1.2 The Introduction of Connectionless Features

During the course of our project, we became aware of the potential to eliminate the need for establishing direct connections before the credential issuance and verification processes. The **connectionless** approach, which was developed in direct response to our requests for streamlining credential workflows, allows for the exchange of credentials and their verification without a persistent connection between parties.

The introduction of this functionality brought two key features:
1. **Connectionless Credential Issuance**: Admins can request an invitation from Issuers for connectionless issuance, which is passed on to the holder to accept and get issued a verified credential.
2. **Connectionless Verification of VCs**: Admins can request an invitation from Verifiers from connectionless presentation and verification, which is passed on to the holder to accept and present their credentials for verification.

This new model offered significant advantages:
- **Simplified Workflow**: No need for explicit connection setup before issuing or verifying VCs.
- **Asynchronous Operations**: Allowed for credential issuance, presentation and verification, even when parties were not online at the same time.
- **Improved Scalability**: Reduced dependencies on network availability, enhancing the system's flexibility in various conditions.

### 1.3 Why We Changed Our Approach

After recognizing the potential benefits of the connectionless model, we pivoted from focusing on holder-initiated connections to utilizing and optimizing **connectionless credential issuance and verification**. This shift enabled us to meet our initial project goals more efficiently and aligned with the latest technological advances in the Identus ecosystem.

---

## 2. Work Done: Leveraging Connectionless Credential Issuance and Verification

Once we adopted the connectionless approach, our work concentrated on integrating and refining these features across the SDK and Agent repositories, contributing to code, tests, and documentation.

## Updated Documentation

### SDK Docs

1. https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/294 (merged)
2. https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/305

### Identus Docs

1. https://github.com/hyperledger/identus-docs/pull/159 (merged)
2. https://github.com/hyperledger/identus-docs/pull/196

### Cloud Agent Docs

1. https://github.com/hyperledger/identus-cloud-agent/pull/1445
2. https://github.com/hyperledger/identus-cloud-agent/pull/1447 (merged)
3. https://github.com/hyperledger/identus-cloud-agent/pull/1448

## Updated Code

### SDK Code

1. https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/293 (merged)
2. https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/272 (merged)

### Cloud Agent Code

1. https://github.com/hyperledger/identus-cloud-agent/pull/1301 (merged)

## Updates Tests

### SDK Tests

1. Add connectionless end-to-end tests
- PR: https://github.com/hyperledger/identus-edge-agent-sdk-ts/pull/342


### Cloud Agent Tests

1.  Add integration test for conectionless issuance
- PR: https://github.com/hyperledger/identus-cloud-agent/pull/1395
- Scenarios:
  - Connectionless issuance of JWT credential using OOB invitation
    - src: https://github.com/hyperledger/identus-cloud-agent/pull/1395/files#diff-5671bb671c862b658c9861bc8911ab2c7f104358e0305d1424b1e67d34d5e8a5R44
    - test passing: https://github.com/hyperledger/identus-cloud-agent/actions/runs/11382542371/job/31666252907#step:11:690


  - Connectionless issuance of sd-jwt credential with holder binding
    - src: https://github.com/hyperledger/identus-cloud-agent/pull/1395/files#diff-1c6c8c836fe8986fa3f10a7e5eeb4735a2a5c530dfd6afde533af7fa9c0443e1R26
    - test passing: https://github.com/hyperledger/identus-cloud-agent/actions/runs/11382542371/job/31666252907#step:11:924

2. Add test connectionless verification
- PR: https://github.com/hyperledger/identus-cloud-agent/pull/1418
- Scenarios:
  - Connectionless Verification Holder presents jwt credential proof to verifier
    - src: https://github.com/hyperledger/identus-cloud-agent/pull/1418/files#diff-8ac57140b8fa267ddb74f39a562efc29f8596a3451a7baf4127ec3d4a7aec1c2R28
    - test passing: https://github.com/hyperledger/identus-cloud-agent/actions/runs/11551230658/job/32147831076#step:11:797
  - Holder presents sd-jwt proof to <verifier>
    - src: https://github.com/hyperledger/identus-cloud-agent/pull/1418/files#diff-9874cb014033f13e910177ed4f876b77a2a48698a66e9d702e73ac182b644c24R28
    - test passing: https://github.com/hyperledger/identus-cloud-agent/actions/runs/11551230658/job/32147831076#step:11:533

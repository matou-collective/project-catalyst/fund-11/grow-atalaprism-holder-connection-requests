# Research Feedback Report

### 1. **Introduction**

This report documents the research findings and decisions made during the design and development of a new connection process for requesting and issuing verifiable credentials using the Identus SDK and Agent technologies, adding important functionality to enhance user experience and broaden accessibility.

The design sessions during this phase included extensive discussions with the Identus development team, the Ahau wallet application team, and key stakeholders to identify the challenges, possible solutions, and to define the feature and development process.

### 2. **Background and Initial Proposal**

At the start of the project, the initial design approach focused on *a holder-initiated connection process*, where we would reduce the process for establishing a DIDComm connection between credential issuers and wallet holders. This was based on the assumption that it would simplify credential exchanges by creating a simpler process.

However, after gathering input from stakeholders and analyzing the potential limitations of the simplified connection model, the team began to consider alternative approaches, leading to the decision to pivot towards a *connectionless credential issuance and verification feature*. This would enable credential issuance without requiring a formal connection or DIDComm protocol, aiming to enhance flexibility and reduce friction for end users.

### 3. **Stakeholder Feedback and Design Sessions**

#### 3.1 *Stakeholder Design Input Sessions (Phase 1)*

During milestone one of the project, we held a stakeholder design input session. The discussions focused on enabling holder-initiated connections, technical considerations for verification and security, infrastructure requirements for issuers, and potential collaboration opportunities. Recommendations emerging from these discussions included prioritizing user-centric design in SDKs, implementing robust verification mechanisms, ensuring secure infrastructure for issuers, fostering community collaboration, and continuing research and development to enhance user autonomy in credential verification processes. These insights and recommendations formed the foundation for further development and refinement of the connectionless credential issuance feature, emphasizing the importance of user experience, security, and flexibility in the evolving landscape of decentralized identity systems.

For more information on these sessions, you can see the research report here via this link: [Research Report - Phase 1](https://gitlab.com/matou-collective/project-catalyst/fund-11/grow-atalaprism-holder-connection-requests/-/blob/main/milestone-1/research-report.md?ref_type=heads#workshop-research-report-holder-connection-requests)

#### 3.2 *Development Team Design Sessions (Phase 2)*

In the follow-up design research sessions with the Identus development team, discussions centered on feasibility, technical challenges, and alignment with user needs. Key outcomes included:

- **Simplification of the Credential Flow**: Adopting a connectionless approach would simplify the credential issuance process by removing the need to establish a connection before credentials are sent. This decision was seen as more aligned with the primary goal of enabling smooth, decentralized credential exchanges.
- **Security Considerations**: While a connectionless model was preferred from a user experience standpoint, additional security layers (e.g., cryptographic proofs, signature verification, etc.) would be integrated to ensure that credentials are issued securely and accurately.
- **Connectionless Limitations**: Connectionless issuance requires there to be an already established method of communication between the issuer and holder.
- **Integration with Wallet Applications**: Both the Identus team and the Ahau wallet team agreed that this model would require enhanced integration but offered more flexibility for developers, enabling a smoother implementation of future features and scalability.

### 4. **Decision Rationale**

The decision to adopt a connectionless credential issuance feature was driven by several key factors:

- **User-Centric Approach**: The connectionless model was found to align more closely with user expectations for simplicity and ease of use. By reducing friction and avoiding the need for multiple, persistent connections, this approach supports a smoother, more intuitive user experience.
- **Increased Flexibility and Interoperability**: The connectionless feature offers broader compatibility with other decentralized identity systems and wallet applications, improving overall interoperability. This is crucial for ensuring that the system can scale across different ecosystems.
- **Operational Efficiency**: Eliminating the need for auto-connection reduces both the complexity of maintaining connections and the overhead of managing these connections across numerous issuers and wallet holders. This streamlined approach also enhances scalability as the system grows.
- **Security and Trust**: Although connectionless, the model includes necessary security mechanisms to ensure the integrity and authenticity of credentials issued. The development team outlined plans to integrate strong cryptographic safeguards that maintain trust without requiring a formal connection protocol.

The majority of the feedback and discussions were handled in a private Discord channel; however, we have notes from an initial follow-up feedback call, which can be seen via the following link: [Feedback Session Call Notes](https://hackmd.io/EqeDJ5ZySMecybV2_6ErSg?view)

### 5. **Milestone Change Request**

In light of these findings and decisions, the team has submitted a change request to update the milestone 2 objectives and outcomes. The key updates to the milestone include:

- **New Feature Focus**: Shift from a holder-initiated connection process to a connectionless credential issuance process.
- **User-Centric Design**: Incorporation of a simplified user experience, with an emphasis on reducing friction for wallet holders and issuers alike.
- **Technical Design Adjustments**: Updates to the Identus SDK and Agent repositories will be made to incorporate the new connectionless model.
- **Development Process**: Due to project limitations and the need for core infrastructure changes to Identus technologies, it was decided that the Identus team would handle the Identus technology changes while the project team would focus on wallet integration, testing, and documentation.

### 6. **Next Steps and Ongoing Work**

Following the adoption of the connectionless credential issuance feature, the next steps include:

- Finalizing the integration and technical specifications with the Ahau wallet application and other potential wallet vendors.
- Coordinating and implementing the required development with the Identus and Ahau wallet repositories.
- Updating user and developer documentation to reflect the new credential issuance model and ensure smooth adoption.
- Updating the software testing to ensure that current features are undisturbed by the changes and new features are working as designed.

The project team will continue to engage with the Identus team and the development community to refine the feature and ensure its success in practical, real-world use cases.

### 7. **Conclusion**

The decision to adopt a connectionless credential issuance feature represents a strategic pivot that prioritizes user experience, scalability, and interoperability. While the original holder-initiated connections model had its merits, the shift to a connectionless approach aligns better with the goals of enabling seamless, decentralized credential exchanges without unnecessary complexity. The team is now focused on implementing the necessary technical adjustments to bring this vision to life and ensure successful integration with wallet applications.

### 8. **References**
- Project information: [Project Information - Catalyst](https://projectcatalyst.io/funds/11/cardano-open-developers/grow-atalaprism-holder-connection-requests)
- Current wallet connection flow: [Current Flow](https://hackmd.io/jSoMRLqjRnynQpAwaseNJg?view)
- Credential issuance process: [Credential Issuance Process](https://github.com/hyperledger/aries-rfcs/blob/main/features/0036-issue-credential/credential-issuance.png)
- Research report session one: [Session 1 Research Report](https://gitlab.com/matou-collective/project-catalyst/fund-11/grow-atalaprism-holder-connection-requests/-/blob/main/milestone-1/research-report.md?ref_type=heads#workshop-research-report-holder-connection-requests)
- Feedback session call notes: [Call Notes](https://hackmd.io/EqeDJ5ZySMecybV2_6ErSg?view)

Prepared for Project Catalyst Project [Grow AtalaPrism: Holder connection requests](https://projectcatalyst.io/funds/11/cardano-open-developers/grow-atalaprism-holder-connection-requests) by: Ben Tairea

# The Shift from Holder-Initiated Connections to Connectionless Issuance and Verification

## Introduction

This document details the evolution of our connection management process for issuing, presenting and verifying verifiable credentials (VCs). Initially, we sought to move away from the existing, manual cloud agent connection method to a more efficient holder-initiated connection model. However, after engaging with the Atala Prism team and requesting improvements, they developed a connectionless approach that far surpassed our expectations. Upon reviewing their implementation, we realised the connectionless model better met our goals, leading us to adopt this solution over our original holder-initiated plan.

---

## 1. **Current Connection Methodology**

**Overview**

The current method for establishing connections between a Peer (Tribal Member) and an Issuer (Cloud Agent) is manual and involves significant involvement from the Admin (Tribal Kaitiaki). The process requires multiple steps to establish a connection, and issue credentials.

**Issuance User Flow (in Āhau)**

```mermaid
sequenceDiagram
  autonumber

  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Issuer

  Peer -->> Kaitiaki: Here is my registration

  Kaitiaki -->> Peer: add-member

  rect rgb(255, 255, 255)
    note over Peer, Issuer: Establish Connection

    Kaitiaki ->> +Issuer: request connection invitation (OOB)
    Issuer ->> -Kaitiaki: connection invitation (OOB)

    Kaitiaki -->> Peer: CRED_CONN_INVITE_SENT

    Peer ->> +Issuer: accept connection invitation
    Issuer ->> -Peer: OK

    Peer -->> Kaitiaki: CRED_CONN_COMPLETE
  end

  rect rgb(255, 255, 255)
    note over Peer, Issuer: Issue Credential

    Kaitiaki ->> +Issuer: make credential offer
    Issuer ->> -Kaitiaki: OK

    Kaitiaki -->> Peer: CRED_OFFER_SENT

    Issuer ->> Peer: Credential Offer
    Peer ->> Issuer: accept credential offer
    Issuer ->> Peer: Verifiable Credential

    Peer -->> Kaitiaki: CRED_COMPLETE
  end

```

**Steps Involved**

- **Registration**: The Peer provides registration details to the Admin.
- **Admin-led connection**: The Admin requests a connection invitation from the Issuer, which the Peer then accepts to establish a connection.
- **Credential issuance**: Once connected, the Admin initiates the credential issuance process, and the Issuer offers the credential for the Peer to accept.

**Challenges**

- **Manual process**: The Admin must manage multiple interactions, making it labor-intensive and slow.
- **Limited scalability**: The back-and-forth nature of the process doesn’t scale well for large groups of tribal members.
- **Time-consuming**: Several steps rely on coordination between multiple parties, slowing down the credentialing process.
- **Connectivity issues**: The existing connection model does not cater to the needs of remote tribal communities in Aotearoa, particularly those with low connectivity. These communities often face challenges in establishing reliable connections, making the current model impractical.

---

## 2. **Proposed Holder-Initiated Connection Model**

**Overview**

To streamline this process, we initially proposed a holder-initiated connection model. In this design, the Peer would take charge of establishing the connection with the Issuer directly, reducing the Admins involvement and expediting the workflow.

**Issuance User Flow (in Āhau)**

```mermaid
sequenceDiagram
  autonumber

  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Issuer

	Kaitiaki -->> Peer: Provide DID on the Tribe
  Peer -->> Kaitiaki: Here is my registration

  Kaitiaki -->> Peer: add-member

  rect rgb(255, 255, 255)
    note over Peer, Issuer: Establish Connection

    Peer ->> +Issuer: request connection using DID
    Issuer ->> -Peer: connection invitation (OOB)

    Peer ->> +Issuer: accept connection invitation
    Issuer ->> -Peer: OK

    Peer -->> Kaitiaki: CRED_CONN_COMPLETE
  end

  rect rgb(255, 255, 255)
    note over Peer, Issuer: Issue Credential

    Kaitiaki ->> +Issuer: make credential offer
    Issuer ->> -Kaitiaki: OK

    Kaitiaki -->> Peer: CRED_OFFER_SENT

    Issuer ->> Peer: Credential Offer
    Peer ->> Issuer: accept credential offer
    Issuer ->> Peer: Verifiable Credential

    Peer -->> Kaitiaki: CRED_COMPLETE
  end

```

**Steps Involved**

- **Peer-initiated connection**: The Peer uses a DID provided by the Admin (this cloud be the Issuer DID) to request a connection with the Issuer.
- **Connection acceptance**: The Issuer sends an out-of-band (OOB) connection invitation, which the Peer accepts.
- **Credential issuance**: After the connection is established, the Admin triggers a credential offer, which the Peer can accept.

**Advantages**

- **Reduced Admin involvement**: The Peer initiates the connection, reducing the workload on the Admin.
- **Increased efficiency**: Fewer interactions between parties speed up the process.

**Challenges**

- **Complexity for Peers**: Shifting responsibility to the Peer introduced potential issues, as not all tribal members might be comfortable managing this themselves.
- **Still involved multiple steps**: Though more efficient than the existing method, the holder-initiated model still involved significant back-and-forth interactions between Peer and Issuer.
- **Remote community limitations**: The holder-initiated model still may not effectively address the challenges faced by remote tribal communities with low connectivity, as establishing direct connections may be challenging in these environments.

---

## 3. **Shift to Connectionless Implementation**

**Overview**

After collaborating with the Atala Prism team on implementing our proposed holder-initiated model, they developed a connectionless approach that dramatically simplified the process. Upon reviewing their implementation, we recognised the advantages and pivoted to adopt this method.

**Issuance User Flow (in Āhau)**

```mermaid
sequenceDiagram
  autonumber

  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Issuer

  Peer -->> Kaitiaki: here is my registration

  Kaitiaki -->> Peer: add-member

  rect rgb(255, 255, 255)
    note over Peer, Issuer: Set up Credential Offer
    Kaitiaki->> +Issuer: request VC issuance invitation (OOB)
    Issuer->> -Kaitiaki: VC issuance invitation

    Kaitiaki-->>Peer: CRED_INVITE_SENT
  end

  rect rgb(255, 255, 255)
    note over Peer, Issuer: Issue Credential
    Peer ->> +Issuer: accept VC issuance invitation
    Issuer ->> -Peer: OK
    Peer -->> Kaitiaki: CRED_OFFER_SENT
    Issuer ->> Peer: Credential Offer
    Peer ->> Issuer: accept credential offer
    Issuer ->> Peer: Verifiable Credential
    Peer -->> Kaitiaki: CRED_COMPLETE
  end

```

**Steps Involved**

- **Admin initiates VC issuance**: The Kaitiaki requests a VC issuance invitation from the Issuer.
- **Issuance invitation**: The Issuer sends an out-of-band (OOB) credential issuance invitation, which the Admin forwards to the Peer.
- **Peer accepts invitation**: The Peer accepts the VC issuance invitation directly, triggering the credential offer process.
- **Credential issuance**: The Issuer provides the credential offer to the Peer, who accepts it and receives their verifiable credential.

**Advantages**

- **Eliminates the need for connections**: The Peer no longer has to establish a persistent connection with the Issuer, simplifying the process significantly.
- **Minimal Admin involvement**: The Admin’s role is limited to forwarding invitations, allowing them to focus on other tasks.
- **Scalability**: The connectionless model is highly scalable and efficient, reducing communication overhead and manual coordination.
- **Faster and simpler**: By eliminating back-and-forth steps, this method significantly speeds up credential issuance.
- **Accommodates low connectivity**: This approach is particularly beneficial for remote tribal communities with limited connectivity, as it reduces reliance on stable connections for the credentialing process.

---

## 4. **Why We Transitioned from Holder-Initiated to Connectionless**

**Challenges with Holder-Initiated Approach**

While the holder-initiated model addressed some of the inefficiencies in the existing system, it still required Peers to manage key steps in the connection process, which added complexity for tribal members. Additionally, it did not fully remove the back-and-forth interactions that slowed down the overall process. Moreover, it struggled to meet the needs of remote tribal communities, where connectivity issues could hinder the effectiveness of this model.

**Atala Prism’s Connectionless Approach**

The connectionless implementation developed by the Atala Prism team was a direct response to our needs for a more efficient system. This solution:

- **Reduced complexity for Peers**: By removing the requirement for Peers to manage connections, it made the process easier for tribal members.
- **Eliminated unnecessary steps**: The connectionless model significantly cut down the number of interactions, making the entire process more efficient.
- **Enhanced scalability**: Without the need for managing persistent connections, the model proved to be far more scalable, which is crucial for larger deployments.
- **Addressed connectivity issues**: The connectionless model’s reliance on invitations rather than connections made it much more suitable for tribal communities with limited access to reliable connectivity.

After reviewing their implementation, we concluded that the connectionless approach was superior to the holder-initiated plan. It better aligned with our goals of reducing manual interactions, improving the user experience, and increasing the overall efficiency of the credentialing process.

---

## 5. **Conclusion**

Our journey from the current manual cloud agent connection method, through the proposed holder-initiated approach, and finally to the connectionless model reflects our commitment to finding the most effective and user-friendly solution. The connectionless model has proven to be the most efficient, scalable, and accessible approach, simplifying the credentialing process for both tribal members and admins.

This transition represents a significant improvement in our ability to issue verifiable credentials quickly and at scale, ultimately benefiting the diverse communities we aim to serve.

---

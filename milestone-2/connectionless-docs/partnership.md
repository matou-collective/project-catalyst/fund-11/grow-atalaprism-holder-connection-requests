# 4. Partnership Proposal: Holder-initiated Connections for Credential Issuance and Verification with Atala Prism

## Overview

The team at **Mātou** proposes a partnership with the **Atala Prism** team, to streamline the credential issuance and verification process to suit the needs of remote Maori communities and the wider community. Our project, funded by **Project Catalyst**, focuses on developing a “holder-initiated connection” system that allows whānau and communities to manage their credentials without the needing real-time connections or out-of-band (OOB) invitations.

Integrating Atala Prism’s decentralised identity solutions within Āhau will simplify and secure credential management and verification, benefiting tribal communities and other organisations.

---

## Partnership Objectives

1. **Simplify Credential Management Processes**
    - **Objective**: Enhance and streamline credential issuance and verification by implementing. holder-initiated connections, allowing for efficient offline management of credentials.
    - **Activities:**
        - Develop a system where whānau can receive and verify credentials without requiring OOB invitations or real-time connections.
        - Utilise Atala Prism’s technology to facilitate streamlined credential management, improving accessibility for tribal communities and other organisations.
2. **Integration of Holder-Initiated Connection**
    - **Objective**: Develop a system within Āhau where whānau can directly connect with credentialing agents using decentralised identifiers (DIDs).
    - **Activities**
        - Integrate the Identus Edge Agent SDK to support holder-initiated connections.
        - Ensure compatibility with Āhau’s existing architecture, particularly with Pātaka for database synchronisation and the Identus Edge Agent SDK for decentralised identity management.
3. **Technical and Support Collaboration**
    - **Objective**: Provide technical guidance and support for successful integration.
    - **Activities**:
        - Assist with integrating the holder-initiated connection features into the Atala Prism ecosystem
        - Offer support for testing, troubleshooting, and optimising the system to ensure reliability and performance
4. **Performance and Security Optimisation**
    - **Objective**: Ensure the system can handle large volumes of transactions securely
    - **Activities**:
        - Optimise the holder-initiated connections approach for scalability
        - Work together on security and privacy compliance to meet industry standards and protect sensitive data

---

## What We’ll Need From Atala Prism

1. **Building the Holder-Initiated Connection Feature**
    - Lead the technical development and integration of holder-initiated connections into the Atala Prism ecosystem, ensuring it supports diverse real-world applications.
2. **Technical Support**
    - Provide ongoing advice and assistance during integration, testing, and deployment phases to ensure seamless implementation and performance.
3. **Performance Optimisation**
    - Collaborate on performance tuning to handle large-scale credential issuance and verification effectively.
4. **Security and Privacy Compliance**
    - Support in meeting all security and privacy requirements, ensuring the system’s reliability and trustworthiness.

---

## Our Contributions as Mātou

1. **Demonstrate Holder-Initiated Connections**
    - **Objective**: Showcase the practical applications of the holder-initiated connection feature for tribal membership and digital identity processes.
    - **Activities**:
        - Develop and present a demo highlighting the ease of issuing and verifications with holder-initiated connections.
2. **Build a Tutorial Repository**
    - **Objective**: Facilitate the implementation of holder-initiated connections for developers.
    - **Activities**:
        - Create a comprehensive tutorial repository with code examples and documentation for various use cases, including iwi membership.
3. **Write a Blog Post**
    - **Objective**: Promote the benefits of holder-initiated credential management and our collaboration with Atala Prism.
    - **Activities**:
        - Draft a blog post detailing our Project Catalyst initiative, the new holder-initiated  feature, and how it empowers communities.
4. **Improving Documentation**
    - **Objective**: Enhance the clarity and usability of documentation for the holder-initiated connection feature.
    - **Activities**:
        - Update and refine documentation based on real-world use and feedback, covering best practices and troubleshooting tips.
5. **Providing Feedback and Reporting Issues**
    - **Objective**: Contribute to the continuous improvement of the system
    - **Activities**:
        - Test the system in real-world scenarios, provide feedback and report any issues to support ongoing enhancements.

---

## **Architecture and Implementation**

### **Key Components:**

- **Āhau**: Decentralised data management app integrating Atala Prism for digital identity management.
- **Pātaka**: Tool for database synchronisation among Āhau users.
- **Identus Edge Agent SDK Agents (Holders)**: Facilitate decentralised identity management and connection requests.
- **Issuer Agents (Identus Cloud Agent)**: Manage credential issuance and connection invitations.
- **Verifier Agents ( Identus Cloud Agent)**: Handle credential verification and presentation requests.
- **Mediator (Identus Mediator)**: Ensures secure communication between holders and cloud agents.

### **Our Original Development Plan:**

- **May 20 - July 10, 2024**: Update and deploy cloud agents, integrate SDK features, and backend changes.
- **July 11 - July 20, 2024**: Submit Milestone 2 Proof of Achievement.
- **July 20 - August 10, 2024**: Update frontend, demonstrate use cases, and deploy updates.
- **August 11 - August 20, 2024**: Submit Final Milestone Proof of Achievement.

### **Development Plan**

**Project Timeline: September 2024 - November 2024**

1. **September 2024: Planning and Coordination**
    - **Activities**:
        - Finalise partnership agreements and detailed project scope with Atala Prism.
        - Align on project objectives, milestones, and deliverables.
        - Set up regular meetings and communication channels with the Atala Prism team.
        - Develop a detailed project roadmap and timeline.
2. **October - November 2024:**
    
    **Development and Integration**
    
    - **Activities**:
        - **Develop Identus Edge Agent SDK:** Implement the holder-initiated connection features within the SDK.
        - **Integrate Identus Edge Agent SDK**: Implement and test the holder-initiated connection feature within the Āhau platform.
        - **Enhance Āhau Tools**: Develop and integrate features for database synchronisation (Pātaka) and connection management.
        - **Develop Identus Cloud Agents**: Update Issuer and Verifier Agents to handle holder-initiated credentialing.
    
    **Testing and Quality Assurance**
    
    - **Activities**:
        - Conduct thorough testing of the holder-initiated credentialing system.
        - Perform security audits and address any vulnerabilities.
        - Optimise performance to handle large volumes of credential issuance and verification.
        - Gather feedback from a selected group of users and make necessary adjustments.
    
    **Final Deployment and Review**
    
    - **Activities**:
        - Deploy the updated Āhau platform with the new holder-initiated feature to production.
        - Finalise documentation, including tutorials and best practices.
        - Launch a blog post and promotional materials showcasing the new features.
        - Review the project outcomes with the Atala Prism team and discuss potential future collaborations.
    
     **Post-Deployment Support**
    
    - **Activities**:
        - Monitor the system’s performance and address any issues.
        - Provide ongoing support and maintenance as needed.
        - Collect and analyse user feedback for continuous improvement.
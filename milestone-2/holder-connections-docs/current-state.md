# Current State of Credential Issuance and Verification in Āhau

### Overview:

In the Āhau application, verified credentials are crucial for managing identity within tribal communities. The current system is structured around a peer-to-peer architecture using Scuttlebutt, which ensures secure and efficient communication among users. Below is an outline of the current state regarding the issuance and verification of verified credentials:

### Key Components:

- **Tribe**: An encrypted group within Āhau, representing a Māori iwi, hapū, or tribal community or Whānau (family) in the real world.
- **Tribal Kaitiaki**: Āhau users who act as the creators and/or real-life administrators of a Tribe. They are responsible for configuring the Tribe to manage the issuance and verification of credentials.
- **Issuers and Verifiers**: Cloud-based agents that handle requests for the issuance and verification of credentials within the Āhau framework.
- **Tribal Members**: Āhau users who register to join a Tribe, where their credentials are either issued or verified. This process is facilitated by the Tribal Kaitiaki in collaboration with the cloud agents.

### Credential Issuance:

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Issuer
  
  Peer->>  Kaitiaki: registration

  Kaitiaki ->> Peer: add-member
  
  rect rgb(191, 223, 255)
    note right of Peer: Establish Connection 

    Kaitiaki ->> +Issuer: make connection inivte (http)
    Issuer -->> -Kaitiaki: oob invite

    Kaitiaki ->> Peer: CONN_INVITE (+oob)

    Peer ->> Issuer: agent.acceptDIDCommInvitation
    Peer ->> Kaitiaki: CONN_COMPLETE
  end
  
  rect rgb(223, 191, 255)
    note right of Peer: Issue Credential
    
    Kaitiaki ->> +Issuer: make cred offer (http)
    Issuer -->> -Kaitiaki: OK

    Kaitiaki ->> Peer: OFFER_SENT
    
    Issuer ->> Peer: Credential Offer
    Peer ->> Issuer: Accept Credential
    Issuer ->> Peer: Verifiable Credential
    Peer ->> Kaitiaki: COMPLETE
  end

```

1. The `Tribal Member` registers to join a `Tribe`
2. The `Tribal Kaitiaki` approves the registration and the `Tribal Member` becomes a member of the `Tribe`
3. The `Tribal Kaitiaki` starts the issuance process by getting an `OOB invitation` from the `Issuer` cloud agent
4. The Issuer sends back the `OOB invitation` 
5. The `Tribal Kaitiaki` passes it on to the `Tribal Member`
6. The `Tribal Member` uses the `OOB invitation` to establish a connection with the `Issuer`
7. The `Tribal Member` updates the registration status as "`connection is complete`"
8. The `Tribal Kaitiaki` receives the status change and makes a credential offer, commanding the `Issuer` to issue an offer
9. The `Tribal Kaitiaki` receives an `OK` from the `Issuer`
10. The `Tribal Kaitiaki` tells the `Tribal Member` that an Offer is on the way
11. The `Issuer` sends an offer, which eventually gets to the `Tribal Member`
12. The `Tribal Member` (who has been listening for the Offer) gets the offer, and accepts it
13. `Issuer` sends the `Verifiable Credential`
14. The `Tribal Member` marks the registration status as `COMPLETE`

### Credential Verification:

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Verifier
  
  Peer->>  Kaitiaki: registration

  rect rgb(191, 223, 255)
    note right of Peer: Establish Connection 

    Kaitiaki ->> +Verifier: make connection inivte (http)
    Verifier -->> -Kaitiaki: oob invite

    Kaitiaki ->> Peer: CONN_INVITE (+oob)

    Peer ->> Verifier: agent.acceptDIDCommInvitation
    Peer ->> Kaitiaki: CONN_COMPLETE
  end
  
  rect rgb(223, 191, 255)
    note right of Peer: Present Credential
    
    Kaitiaki ->> +Verifier: make presentation req. (http)
    Verifier -->> -Kaitiaki: OK

    Kaitiaki ->> Peer: REQUEST_SENT (+presentationId)
    
    Verifier ->> Peer: Presentation Request
    Peer ->> Verifier: Presentation
    Peer ->> Kaitiaki: PRES_SENT
    Kaitiaki ->> +Verifier: get presentation (http)
    Verifier -->> -Kaitiaki: Verified
    Kaitiaki ->> Peer: COMPLETE
  end
  
  Kaitiaki ->> Peer: add-member

```

1. The `Tribal Member` registers to join a `Tribe` (and they have a credential to present)
2. The `Tribal Kaitiaki` starts the verification process by getting an `OOB invitation` from the `Verifier` cloud agent
3. The Issuer sends back the `OOB invitation` 
4. The `Tribal Kaitiaki` passes it on to the `Tribal Member`
5. The `Tribal Member` uses the `OOB invitation` to establish a connection with the `Verifier` 
6. The `Tribal Member` updates the registration status as "`connection is complete`"
7. The `Tribal Kaitiaki` receives the status change and makes a presentation request, commanding the `Verifier` to issue a presentation request
8. The `Tribal Kaitiaki` receives an `OK` from the `Verifier`
9. The `Tribal Kaitiaki` tells the `Tribal Member` that an `Presentation Request` is on the way
10. The `Verifier` sends a presentation request, which eventually gets to the `Tribal Member`
11. The `Tribal Member` (who has been listening for the request) gets the offer, and sends the `Presentation`
12. The `Tribal Member` tells the `Tribal Kaitiaki` that a presentation has been sent `PRES_SENT`
13. The `Tribal Kaitiaki` get requests the presentation status from the `Verifier`
14. The `Verifier` responds that the presentation was `Verified`
15. The `Tribal Kaitiaki` marks the registration status as `COMPLETE` 
16. The `Tribal Member` becomes a member of the `Tribe`

### Integration with Atala Prism:

Āhau leverages the Identus/Atala Prism stack through the following means:

- `identus-edge-agent-sdk-ts`: We’ve built a module `ssb-atala-prism` which integrates the current version `v6.1.0` of the SDK, allowing Āhau users to have an in-app wallet to store credentials issued by tribes within Āhau.
    - This module also includes testing suitable for both local and cloud agents and mediators, to test the functionality and features against our implementation within the Āhau stack.
    - https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism
- `identus-cloud-agent` : We have stood up our own Verifier and Issuer cloud agents running `v1.39.0`
- `identus-mediator`: We have stood up our own Mediator running `0.15.0`

### User Experience:

Users interact with the credential issuance and verification features as follows:

1. **Interaction with Kaitiaki**: `Tribal Kaitiaki` facilitate the issuance and/or verification of credentials, while `Tribal Members` receive and present these credentials through the in-app wallet.
2. **Feedback**: Users have reported that the current back-and-forth communication between `Tribal Kaitiaki` and `Tribal Members` during the registration process is slow, which could hinder usability in areas with poor connectivity.

### Current Status:

The credential issuance and verification features are fully functional but require improvements to streamline the registration process between `Tribal Kaitiaki` and `Tribal Members`. These enhancements are particularly necessary for use cases in remote areas with low connectivity. The system is currently being tested with various tribal partners.

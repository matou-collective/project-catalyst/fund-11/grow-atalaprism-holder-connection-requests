# Holder-Initiated Connections for Digital Credential Verification

### **Overview**

Holder-initiated connections streamline the process of credential verification by allowing users to directly connect with cloud agents. This eliminates the need for out-of-band (OOB) invitations from administrators, empowering users to start the verification process at their convenience and improving the efficiency of credential management.

### **Objective**

To leverage holder-initiated connections for secure and efficient digital credential verification, allowing users to directly connect with cloud agents and present their credentials without requiring admin intervention.

### **Process**

1. **Credential Issuance**:
    - Users are issued digital credentials by a trusted authority or organisation. These credentials include a decentralised identifier (DID) or other connection details that facilitate direct communication with cloud agents.
2. **Connection Details**:
    - The authority or organisation provides users with a DID or a connection endpoint that they can use to initiate a connection with the relevant cloud agents. This information is made available to users through secure channels, such as a mobile app or a secure website.
3. **Initiating Connection**:
    - Users initiate a connection with cloud agents using the provided DID or endpoint. This direct connection allows them to start the verification process without waiting for an admin to send an invitation.
4. **Credential Presentation**:
    - Users present their digital credentials through the established connection. This can be done using a mobile app or other digital tools, where they share the required information with the verifier.
5. **Verification Process**:
    - Cloud agents receive and process the credentials provided by the users. They perform the necessary checks to verify the authenticity and validity of the credentials.
6. **Outcome**:
    - Once verified, users receive confirmation of their credentials' validity. This direct approach enhances the efficiency and user experience, as it eliminates the delays and complexities associated with traditional out-of-band connection methods.

### **Examples**

- **Retail Transactions**: A customer wants to purchase age-restricted items, such as alcohol. They use holder-initiated connections to present their digital ID for age verification without requiring the cashier to initiate the connection.
- **Student ID Verification**: A student needs to verify their identity for access to campus facilities. They initiate a connection with the university's credential system to present their digital student ID and gain access.
- **Access Control**: An employee needs to access secure areas within a building. They initiate a connection with the access control system to present their digital access credentials, streamlining the entry process.

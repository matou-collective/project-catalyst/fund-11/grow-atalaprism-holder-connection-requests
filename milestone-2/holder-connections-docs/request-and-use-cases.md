# 3. Improvement Requests and Use Cases

### Streamlining the Connection Process

**Current Issue:**
The current process involves significant back-and-forth communication between the `Tribal Kaitiaki` and `Tribal Members`. The `Tribal Kaitiaki` must first request an Out-of-Band (OOB) invitation from the cloud agents and then relay this invitation to the members, who use it to establish a connection. This troublesome workflow can be inefficient, especially in remote areas with limited connectivity.

**Proposed Improvement:**
We propose reducing the dependency on the `Tribal Kaitiaki` for handling `OOB invitations`. Instead, `Tribal Members` should be able to initiate the connection process directly by using the `DID` of the relevant cloud agent. This would eliminate the need for Kaitiaki to act as intermediaries for every connection setup, significantly simplifying the process.

**Desired Flow:**

1. **Holder-Initiated Connection**: `Tribal Members` should be provided with the `DID` of the `Issuer` or `Verifier` cloud agent.
2. **Self-Service Request**: Using the `DID`, the member can independently request a connection from the cloud agent without needing an `OOB invitation` from the `Tribal Kaitiaki`.
3. **Direct Establishment**: The cloud agent sends the connection invitation directly to the member, who can then establish the connection.
4. **Notification**: `Tribal Kaitiaki` are notified once the connection is established, allowing them to proceed with issuing or verifying credentials as needed.

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Issuer
  
  Kaitiaki->>Peer: Provide DID on the Tribe

  rect rgb(191, 223, 255)
    note right of Peer: Establish Connection

    Peer ->> Issuer: Request connection using DID
    Issuer -->> Peer: Connection Invite
    Peer ->> Issuer: Accept Invitation
    Peer ->> Kaitiaki: Connection Complete
  end

  rect rgb(223, 191, 255)
    note right of Peer: Issue Credential
    
    Kaitiaki ->> +Issuer: Make credential offer (http)
    Issuer -->> -Kaitiaki: OK

    Kaitiaki ->> Peer: OFFER_SENT
    
    Issuer ->> Peer: Credential Offer
    Peer ->> Issuer: Accept Credential
    Issuer ->> Peer: Verifiable Credential
    Peer ->> Kaitiaki: COMPLETE
  end

```

1. **Establish Connection:**
    - The `Kaitiaki` provides the DID of the cloud agent on the `Tribe`
    - The `Tribal Member` uses the DID to request a connection directly from the `Issuer`.
    - The `Issuer` sends a connection invitation, which the `Tribal Member` accepts, completing the connection process.
2. **Issue Credential:**
    - The `Kaitiaki` initiates a credential offer request to the `Issuer`.
    - The `Issuer` sends an offer to the `Tribal Member`.
    - The `Tribal Member` accepts the credential offer, and the `Issuer` issues the Verifiable Credential.
    - The `Tribal Member` informs the `Kaitiaki` that the process is complete.

**Benefits:**

- **Reduced Manual Steps**: Simplifies the process, minimising the number of steps and reducing the potential for errors.
- **Improved User Experience**: `Tribal Members` can complete the connection process more autonomously, reducing wait times and enhancing usability, particularly in low-connectivity scenarios.
- **Scalability**: Makes the system more scalable by reducing the burden on `Tribal Kaitiaki`, allowing them to focus on higher-level administrative tasks.

Additional Use Case:

[**Use Case: Holder-Initiated Connections for Digital Credential Verification**](./use-case-2.md)

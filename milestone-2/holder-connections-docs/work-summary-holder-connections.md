# Summary of Work on Holder-Initiated Connections 

### Overview

Our work on holder-initiated connections is a key component of our initiative funded by Project Catalyst. This project aims to enhance credential management and verification processes for remote Māori communities and the broader community by implementing a holder-initiated connection system. This system simplifies credential handling without requiring real-time connections or out-of-band (OOB) invitations.

### Project Catalyst

Our project, **“Grow AtalaPrism Holder Connection Requests”**, has been funded by Project Catalyst. This initiative focuses on developing a holder-initiated connection system to streamline credential management and verification.

- Links:
    - **Project Catalyst Funding Page**: [Grow AtalaPrism Holder Connection Requests](https://projectcatalyst.io/funds/11/cardano-open-developers/grow-atalaprism-holder-connection-requests)
    - **Milestone 1:** https://milestones.projectcatalyst.io/projects/1100149/milestones/1
    - **Milestone 2:** https://milestones.projectcatalyst.io/projects/1100149/milestones/2
    - **Final Milestone:** https://milestones.projectcatalyst.io/projects/1100149/milestones/3
    - **Milestone Documents and outputs:** https://gitlab.com/matou-collective/project-catalyst/fund-11/grow-atalaprism-holder-connection-requests/-/tree/main?ref_type=heads

## Meetings

1. **Community Workshop (May, 9 2024)**
    - **Summary:** A workshop held with members of the Atala Prism community, around the need of holder-initiated connections.
    - **Key Points:** See Workshop research report below
    - **Meeting Notes:** https://hackmd.io/9g3FLXzZQnqiFeZjYc4Fkw?view
2. **Meeting with Atala Prism Team (August 28, 2024)**
    - **Summary**: Explored enabling “edge” agents to initiate connections with cloud agents, discussed connectionless OOB invitations, and the issuance process.
    - **Key Points**:
        - Long term goals include enhancing user experience and partnering with Identus.
        - Connectionless flow and potential DDOS issues with OOB invites.
        - Issuance process overview including admin approvals and VC requests.
        - Proposal for collaboration on “connectionless issuance” with Mātou contributing to use cases, testing, documentation and tutorials
    - **Meeting Notes**: https://hackmd.io/EqeDJ5ZySMecybV2_6ErSg?view

### Documents Written

1. **Workshop research report: Holder Connection Requests (May 2024)**
    - **Summary**: The workshop explored how SDKs can enable holders to initiate connections independently, highlighting user autonomy, secure verification, and robust infrastructure needs. Recommendations included enhancing SDK design, implementing secure verification, and fostering community collaboration.
    - **Key Sections**:
        - Use Cases: Direct holder initiation importance.
        - Technical Aspects: Trust registries, public key verification, secure issuance.
        - Infrastructure: Secure node operation, multsig.
    - **Document Link:** https://gitlab.com/matou-collective/project-catalyst/fund-11/grow-atalaprism-holder-connection-requests/-/blob/main/milestone-1/research-report.md?ref_type=heads
2. **Product Requirements Document (May 2024)**
    - **Summary**: Outlines the requirements for extending the Atala Prism Wallet SDK to enable holders to initiate connections with cloud agents.
    - **Key Sections:**
        - System Architecture within Ahau
        - User Stories:
            - Holders initiating connections directly
            - Cloud agents receiving and accepting invitations
            - Admins configuring cloud agents
        - Current vs. Improved Connections Process
        - Use Cases:
            - Real-world example: Proactive credential offering at a supermarket.
            - Āhau integration: Streamlining tribal registration and connection processes.
        - Requirements, Risks and Testing and Quality Assurance
    - **Document Link:** https://gitlab.com/matou-collective/project-catalyst/fund-11/grow-atalaprism-holder-connection-requests/-/blob/main/milestone-1/requirements.md?ref_type=heads&plain=0
3. **Product Development Plan Document (May 2024)**
    - **Summary**: Outlines the stages of product development, including updates and deployments for cloud agents, integrations with the Atala Prism Wallet SDK, and frontend and backend updated for Āhau, with detailed tasks and timelines for each stage.
    - **Document Link:** https://gitlab.com/matou-collective/project-catalyst/fund-11/grow-atalaprism-holder-connection-requests/-/blob/main/milestone-1/development-plan.md?ref_type=heads

### Pull Requests Opened

1. **ssb-atala-prism - PR #2: Initiate Connection WIP**
    - **Summary:** A rough sketch of sending a message to a cloud agent from the Āhau’s edge agent implementation.
    - **PR Link:** https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/merge_requests/2
2. **ssb-atala-prism - PR #3: Update SDK from v5.1.0 to v5.2.0**
    - **PR Link:** https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/merge_requests/3
3. **ssb-atala-prism** **- PR #5: Update SDK to identus-edge-agent-sdk v6.0.0**
    - **PR Link:** https://gitlab.com/matou-collective/lib/atala-prism/ssb-atala-prism/-/merge_requests/5

### Issues Created on GitHub

1. **Identus Cloud Agent - Issue #1163: Edge-client initiated connections**
    - **Summary**: Proposes feature to enable cloud agents to accept connection requests from holders by either crafting a compatible message or modifying the agent to accept these connections
    - **Issue Link:** https://github.com/hyperledger/identus-cloud-agent/issues/1163
2. **Identus Edge Agent SDK TS - Issue #118: Holder initiated Presentations**
    - **Summary:** Proposes feature to enable holders to initiate presentations
    - **Issue Link:** https://github.com/hyperledger/identus-edge-agent-sdk-ts/issues/118